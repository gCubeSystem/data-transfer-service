package org.gcube.data.transfer.service;

import javax.ws.rs.ApplicationPath;

import org.gcube.data.transfer.model.ServiceConstants;
import org.gcube.data.transfer.service.transfers.Capabilities;
import org.gcube.data.transfer.service.transfers.REST;
import org.gcube.data.transfer.service.transfers.Requests;
import org.gcube.data.transfer.service.transfers.TransferStatus;
import org.gcube.data.transfer.service.transfers.engine.AccountingManager;
import org.gcube.data.transfer.service.transfers.engine.CapabilitiesProvider;
import org.gcube.data.transfer.service.transfers.engine.PersistenceProvider;
import org.gcube.data.transfer.service.transfers.engine.PluginManager;
import org.gcube.data.transfer.service.transfers.engine.RequestManager;
import org.gcube.data.transfer.service.transfers.engine.TicketManager;
import org.gcube.data.transfer.service.transfers.engine.factories.AccountingManagerFactory;
import org.gcube.data.transfer.service.transfers.engine.factories.CapabilitiesProviderFactory;
import org.gcube.data.transfer.service.transfers.engine.factories.PersistenceProviderFactory;
import org.gcube.data.transfer.service.transfers.engine.factories.PluginManagerFactory;
import org.gcube.data.transfer.service.transfers.engine.factories.RequestManagerFactory;
import org.gcube.data.transfer.service.transfers.engine.factories.TicketManagerFactory;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ApplicationPath(ServiceConstants.APPLICATION_PATH)
public class DTService extends ResourceConfig{

	public DTService() {
		super();
		log.warn("Going to define Binder ");
		try {
			AbstractBinder binder = new AbstractBinder() {
				@Override
				protected void configure() {                    
					bindFactory(AccountingManagerFactory.class).to(AccountingManager.class);
					bindFactory(CapabilitiesProviderFactory.class).to(CapabilitiesProvider.class);
					bindFactory(PersistenceProviderFactory.class).to(PersistenceProvider.class);
					bindFactory(PluginManagerFactory.class).to(PluginManager.class);
					bindFactory(RequestManagerFactory.class).to(RequestManager.class);
					bindFactory(TicketManagerFactory.class).to(TicketManager.class);
					
					

					
				}
			};
			register(binder);

			registerClasses(Capabilities.class);
			registerClasses(Requests.class);
			registerClasses(REST.class);
			registerClasses(TransferStatus.class);
			//		ApplicationContext ctx = ContextProvider.get();

			//		ctx.application().addListener(Listener.class.getName());


			packages("org.gcube.data.transfer.service.transfers");
			packages("org.glassfish.jersey.media.multipart");
			packages("org.glassfish.jersey.media.multipart.internal");
			//		register(ProviderLoggingListener.class);
			register(MultiPartFeature.class);
		}catch(Throwable t) {
			log.error("Init error ",t );
			throw t;
		}
	}

}
