package org.gcube.data.transfer.service.transfers.engine.factories;

import org.gcube.data.transfer.service.transfers.engine.RequestManager;
import org.gcube.data.transfer.service.transfers.engine.impl.RequestManagerImpl;
import org.glassfish.hk2.api.Factory;

public class RequestManagerFactory implements Factory<RequestManager>{

	
	@Override
	public void dispose(RequestManager instance) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public RequestManager provide() {
		return new RequestManagerImpl(new TicketManagerFactory().provide(), 
				new PersistenceProviderFactory().provide());
	}
}
