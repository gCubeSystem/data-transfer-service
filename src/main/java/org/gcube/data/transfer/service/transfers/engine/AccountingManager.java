package org.gcube.data.transfer.service.transfers.engine;

public interface AccountingManager {

	public String createNewRecord();
	public void account(String toAccountRecordId);	
	
	
	public void setSuccessful(String id,boolean succesfull); 
	public void setRead(String id);
	public void setCreate(String id);
	public void setDelete(String id);
	public void setUpdate(String id);
	public void setResourceURI(String id,String uri);
	public void setVolumne(String id, long volume);
	public void setMimeType(String id,String mimeType);
	
	
	

}
