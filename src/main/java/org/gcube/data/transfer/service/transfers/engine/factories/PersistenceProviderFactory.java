package org.gcube.data.transfer.service.transfers.engine.factories;

import org.gcube.data.transfer.service.transfers.engine.PersistenceProvider;
import org.gcube.data.transfer.service.transfers.engine.impl.PersistenceProviderImpl;
import org.glassfish.hk2.api.Factory;

public class PersistenceProviderFactory implements Factory<PersistenceProvider>{

	
	public void dispose(PersistenceProvider instance) {};
	
	@Override
	public PersistenceProvider provide() {
		return new PersistenceProviderImpl();
	}
	
}
