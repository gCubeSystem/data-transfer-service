package org.gcube.data.transfer.service.transfers.engine.factories;

import org.gcube.data.transfer.service.transfers.engine.TicketManager;
import org.gcube.data.transfer.service.transfers.engine.impl.TransferTicketManagerImpl;
import org.glassfish.hk2.api.Factory;

public class TicketManagerFactory implements Factory<TicketManager>{

	@Override
	public void dispose(TicketManager instance) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public TicketManager provide() {
		return new TransferTicketManagerImpl();
	}
	
}
