package org.gcube.data.transfer.service.transfers.engine.factories;

import org.gcube.data.transfer.service.transfers.engine.AccountingManager;
import org.gcube.data.transfer.service.transfers.engine.impl.AccountingManagerImpl;
import org.glassfish.hk2.api.Factory;

public class AccountingManagerFactory implements Factory<AccountingManager>{

	@Override
	public void dispose(AccountingManager instance) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public AccountingManager provide() {
		return AccountingManagerImpl.get();
	}
}
