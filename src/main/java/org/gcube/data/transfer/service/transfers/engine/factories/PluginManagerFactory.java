package org.gcube.data.transfer.service.transfers.engine.factories;

import org.gcube.data.transfer.service.transfers.engine.PluginManager;
import org.gcube.data.transfer.service.transfers.engine.impl.PluginManagerImpl;
import org.glassfish.hk2.api.Factory;

public class PluginManagerFactory implements Factory<PluginManager>{

	
	@Override
	public void dispose(PluginManager instance) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public PluginManager provide() {
		return new PluginManagerImpl();
	}
}
