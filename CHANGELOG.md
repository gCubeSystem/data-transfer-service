This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.data.transfer.data-transfer-plugin-framework

## [v2.0.8] 2022-12-06

- Update to integrate last version of common-smartgears-app

## [v2.0.7] 2020-09-07

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
